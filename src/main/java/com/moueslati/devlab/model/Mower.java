package com.moueslati.devlab.model;

import com.moueslati.devlab.behavior.MowerBehaviour;

public class Mower implements MowerBehaviour {
	
	public Mower(Position position, Direction direction) {
		
		this.position = position;
		this.direction = direction;
	}
	
	
	private Position position;
	private Direction direction;
	
	
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}
	public Direction getDirection() {
		return direction;
	}
	public void setDirection(Direction direction) {
		this.direction = direction;
	}
	
	
	@Override
	public String toString() {
		return "Mower [position=" + position + ", direction=" + direction + "]";
	}
	
	
	public Position getCurrentPosition() {
		return this.getPosition();
	}
	public Position getNextPosition() {
		
		Position nextPostion = null;
		switch(direction) {
					
					case North: nextPostion = new Position(position.getX(),position.getY()+1);
								break;
					case East:	nextPostion = new Position(position.getX()+1,position.getY());
								break;
					case South:	nextPostion = new Position(position.getX(),position.getY()-1);
								break;
					case West:	nextPostion = new Position(position.getX()-1,position.getY());
								break;
		}
		return nextPostion;
	}
	
	public Direction getCurrentDirection() {
		
		return this.getDirection();
	}
	
	public void changeDirection(Movement movement) {
		
		switch (movement) {
		case Right:
			this.direction = direction.nextDirection();
			break;
		
		case Left:
			this.direction = direction.previsousDirection();
			break;
			
		default:
			break;
		
		}
	}
	public void move() {
		this.position = getNextPosition();
	}
	
	
	

}
