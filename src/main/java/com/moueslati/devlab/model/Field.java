package com.moueslati.devlab.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.moueslati.devlab.behavior.FieldBehaviour;

public class Field implements FieldBehaviour {
	
	
	public Field(Integer width, Integer length) {
		
		this.width = width;
		this.length = length;
		this.mowers = new ArrayList<Mower>();
		this.mowerToMove = Optional.empty();
	}

	private List<Mower> mowers;
	private Optional<Mower> mowerToMove;
	private Integer width;
	private Integer length;
	
	public Integer getWidth() {
		
		return this.width;
	}

	public Integer getLength() {
		
		return this.length;
	}
	
	public List<Mower> getMowers() {
		
		return mowers;
	}

	public Optional<Mower> getMowerToMove() {
		
		return mowerToMove;
	}

	public boolean isPlaceableIn(Position position) {

		if(position == null)
			return false;
		if (position.getX() > this.width || position.getX() <0 || position.getY()>this.length || position.getY()<0)
			return false;
		return mowers.stream().noneMatch(mow -> mow.getCurrentPosition().equals(position));
	}

	public boolean placeMower(Mower mower) {
		
		if (mower != null && isPlaceableIn(mower.getCurrentPosition()))  {
			
			mowers.add(mower);
			return true;
		}
		return false;
	}

	public void moverMower(List<Movement> movements) {
		
		int mowerToMvIdx = mowers.indexOf(mowerToMove.orElse(null))+1;
		if (mowerToMvIdx>=0) {
			
			mowerToMove = Optional.of(mowers.get(mowerToMvIdx));
			movements.forEach( mv -> moveMowerByStep(mowerToMove.get(),mv));
		}	
	}

	public void reset() {
		mowers.clear();
		mowerToMove = Optional.empty();
	}
	
	private void moveMowerByStep(Mower mower,Movement movement) {
		 
		 switch(movement) {
		 	
			 case Right: mower.changeDirection(Movement.Right);
				 break;
			 case Left: mower.changeDirection(Movement.Left);
				 break;
			 case Forward: if (isPlaceableIn(mower.getNextPosition()))
					 			mower.move();
				 		   break;
		 }
	 }
}
