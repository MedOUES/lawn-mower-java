package com.moueslati.devlab.model;

public class Position {
	
	public Position(Integer x, Integer y) {
		super();
		this.x = x;
		this.y = y;
	}

	private Integer x;
	private Integer y;
	
	
	public Integer getX() {
		return x;
	}
	public void setX(Integer x) {
		this.x = x;
	}
	public Integer getY() {
		return y;
	}
	public void setY(Integer y) {
		this.y = y;
	}
	
	@Override
	public String toString() {
		return "Position [x=" + x + ", y=" + y + "]";
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Position other = (Position) obj;
		if (x == null && other.x != null) 
			return false;
		if (!x.equals(other.x))
			return false;
		if (y == null && other.y != null) 
			return false;
		if (!y.equals(other.y))
			return false;
		
		return true;
	}
}
