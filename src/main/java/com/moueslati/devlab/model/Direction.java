package com.moueslati.devlab.model;

public enum Direction {
	
	North,West,South,East;
	
	public Direction nextDirection() {
		
		Direction nextDirection = null;
			switch (this) {
			
				case North: nextDirection = East;
							break;
				case West:	nextDirection = North;
							break;
				case South: nextDirection = West;
							break;
				case East: 	nextDirection = South;
							break;
			}
		return nextDirection;
	}
	
	public Direction previsousDirection() {
		
		Direction previousDirection = null;
		switch (this) {
		
			case North: previousDirection = West;
						break;
			case West:	previousDirection = South;
						break;
			case South: previousDirection = Direction.East;
						break;
			case East: 	previousDirection = North;
						break;
		}
	return previousDirection;
	}
}
