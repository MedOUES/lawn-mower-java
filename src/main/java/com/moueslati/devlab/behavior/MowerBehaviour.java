package com.moueslati.devlab.behavior;

import com.moueslati.devlab.model.Direction;
import com.moueslati.devlab.model.Movement;
import com.moueslati.devlab.model.Position;

public interface MowerBehaviour {
	
	Position getCurrentPosition();
	Position getNextPosition();
	Direction getCurrentDirection();
	void changeDirection(Movement movement);
	void move();
}
