package com.moueslati.devlab.behavior;

import java.util.List;
import java.util.Optional;

import com.moueslati.devlab.model.Movement;
import com.moueslati.devlab.model.Mower;
import com.moueslati.devlab.model.Position;

public interface FieldBehaviour {
	
	 Integer getWidth();
	 Integer getLength();
	 List<Mower> getMowers();
	 Optional<Mower> getMowerToMove();
	 boolean isPlaceableIn(Position position);
	 boolean placeMower(Mower mower);
	 void moverMower(List<Movement> movements);
	 void reset();
}
