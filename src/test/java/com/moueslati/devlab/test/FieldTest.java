package com.moueslati.devlab.test;

import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.Arrays;
import java.util.Optional;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.moueslati.devlab.model.Direction;
import com.moueslati.devlab.model.Field;
import com.moueslati.devlab.model.Movement;
import com.moueslati.devlab.model.Mower;
import com.moueslati.devlab.model.Position;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FieldTest {

	private static Field field;
	private static Mower mowerA;
	private static Position position0A;
	private static Direction direction0A;
	
	private static Mower mowerB;
	private static Position position0B;
	private static Direction direction0B;
	
	
	@BeforeClass
	public static void initStep() {
		
		field = new Field(7, 10);
		mowerA = new Mower(new Position(0,0),Direction.North);
		position0A = new Position(0, 0);
		direction0A = Direction.North;
		
		mowerB = new Mower(new Position(6,4),Direction.East);
		position0B = new Position(6, 4);
		direction0B = Direction.East;
	}
	
	@Test
	public void test1_InstantiateField() {

		
		assertThat(field.getWidth(),equalTo(7));
		assertThat(field.getLength(),equalTo(10));
	}

	@Test
	public void test2_NoMowers() {

		assertThat(field.getMowers().size(),equalTo(0));
	}

	@Test
	public void test3_NoMowerToMove() {
		
		assertThat(field.getMowerToMove(),equalTo(Optional.empty()));
	}

	@Test
	public void test4_IncreaseNumberOfMower() {
		
		field.placeMower(mowerA);
		field.placeMower(mowerB);
		assertThat(field.getMowers().size(),equalTo(2));
	}

	@Test
	public void test5_IgnoreNonPlaceableMowers() {
		
		field.placeMower(new Mower(new Position(9, 4), Direction.East));
		assertThat(field.getMowers().size(),equalTo(2));
	}

	@Test
	public void test6_MoveTheFirstAddedMower() {
		
		field.moverMower(Arrays.asList(Movement.Forward,Movement.Forward,Movement.Right,Movement.Forward));
		assertThat(field.getMowerToMove().get(),sameInstance(mowerA)); 
	}

	@Test
	public void test7_PlaceInCorrectPosition_KeepDirection() {
		
		assertThat(mowerA.getCurrentPosition(),equalTo(new Position(position0A.getX()+1,position0A.getY()+2)));
		assertThat(mowerA.getDirection(), equalTo(direction0A.nextDirection()));
	}

	@Test
	public void test8_PointingToTheSecondMower() {
		
		field.moverMower(Arrays.asList(Movement.Forward,Movement.Forward,Movement.Right,Movement.Forward));
		assertThat(field.getMowerToMove().get(),sameInstance(mowerB)); 
	}

	@Test
	public void test9_EmptyField() {
		
		field.reset();
		assertThat(field.getMowers().size(),equalTo(0));
		assertThat(field.getMowerToMove(),equalTo(Optional.empty()));
	}
}
